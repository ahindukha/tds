package com.example.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvregister;
    private Button btnsignin;
    private EditText login_email;
    private EditText login_password;

    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvregister = (TextView) findViewById(R.id.register);
        btnsignin = (Button) findViewById(R.id.btnlogin);
        login_email = (EditText) findViewById(R.id.login_email);
        login_password = (EditText) findViewById(R.id.login_password);

        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);

        if(firebaseAuth.getCurrentUser()!=null){
            startActivity(new Intent(getApplicationContext(),HomePage.class));
        }


        tvregister.setOnClickListener(this);
        btnsignin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if(v ==tvregister){
            startActivity(new Intent(getApplicationContext(),RegistrationActivity.class));
        }

        if(v == btnsignin){
           String mEmail = login_email.getText().toString().trim();
           String mPassword = login_password.getText().toString().trim();

           if(TextUtils.isEmpty(mEmail)){
               login_email.setError("Required field");
               return;
           }
           if(TextUtils.isEmpty(mPassword)){
               login_password.setError("Required field");
               return;
           }

           progressDialog.setMessage("Processing...");
           progressDialog.show();

           firebaseAuth.signInWithEmailAndPassword(mEmail,mPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
               @Override
               public void onComplete(@NonNull Task<AuthResult> task) {

                   if(task.isSuccessful()){
                       progressDialog.dismiss();
                       startActivity(new Intent(getApplicationContext(),HomePage.class));
                   }else {
                       Toast.makeText(getApplicationContext(),"Login Failed...",Toast.LENGTH_SHORT).show();
                       progressDialog.dismiss();
                   }

               }
           });

        }


    }
}
