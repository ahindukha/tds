package com.example.myapplication;

import android.content.Intent;
import android.graphics.ColorSpace;
import android.graphics.Matrix;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.bottomappbar.BottomAppBarTopEdgeTreatment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.widget.Toolbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.Model.Data;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.firebase.ui.database.FirebaseRecyclerAdapter;


import java.text.DateFormat;
import java.util.Date;

public class HomePage extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private FloatingActionButton fab;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private RecyclerView recyclerView;


    private String post_key;
    private String name;
    private String description;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        toolbar = findViewById(R.id.toolbar);
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser mUser = mAuth.getCurrentUser();
        String uid = mUser.getUid();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("ALL Data").child(uid);

        recyclerView = findViewById(R.id.recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);


        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("TDS APP");

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        if(v == fab){
            AddData();
        }

    }
    private void AddData(){

        AlertDialog.Builder mydialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());

        View myview = inflater.inflate(R.layout.inputlayout,null);

        mydialog.setView(myview);
        final AlertDialog dialog = mydialog.create();

        dialog.setCancelable(false);

        final EditText name = myview.findViewById(R.id.name);
        final EditText description = myview.findViewById(R.id.description);

        Button btnCancel = myview.findViewById(R.id.btncancel);
        Button btnSave = myview.findViewById(R.id.btnsave);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mName = name.getText().toString().trim();
                String mDescription = description.getText().toString().trim();

                if(TextUtils.isEmpty(mName)){
                    name.setError("Required Field");
                    return;
                }

                if(TextUtils.isEmpty(mDescription)){
                    description.setError("Required Field");
                    return;
                }

                String id = mDatabase.push().getKey();
                String mDate = DateFormat.getDateInstance().format(new Date());

                Data data = new Data(mName,mDescription,id,mDate);
                mDatabase.child(id).setValue(data);
                Toast.makeText(getApplicationContext(),"Data Uploaded",Toast.LENGTH_SHORT).show();

                /////////////////////////////////////////////////////////

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    @Override
    protected void onStart(){
        super.onStart();
        FirebaseRecyclerAdapter<Data,MyViewHolder>adapter=new FirebaseRecyclerAdapter<Data, MyViewHolder>(

                Data.class,
                R.layout.itemlayoutdesign,
                MyViewHolder.class,
                mDatabase
        ) {
            @Override
            protected void populateViewHolder(MyViewHolder viewHolder, final Data model, final int position) {

                viewHolder.setName(model.getName());
                viewHolder.setDescription(model.getDescription());
                viewHolder.setDate(model.getDate());

                viewHolder.mview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        post_key = getRef(position).getKey();
                        name = model.getName();
                        description = model.getDescription();

                        updateData();
                    }
                });

            }
        };

        recyclerView.setAdapter(adapter);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        View mview;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mview = itemView;
        }

        public void setName(String name){
            TextView Mname = mview.findViewById(R.id.name_item);
            Mname.setText(name);

        }

        public void setDescription(String description){
            TextView Mdec = mview.findViewById(R.id.desc_item);
            Mdec.setText(description);
        }

        public void setDate(String date){
            TextView Mdate = mview.findViewById(R.id.date_item);
            Mdate.setText(date);
        }




    }

    public void updateData(){

        AlertDialog.Builder mydialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        View myview = inflater.inflate(R.layout.update_data,null);
        mydialog.setView(myview);

        final AlertDialog dialog = mydialog.create();
        //dialog.setCancelable(false);

        final EditText mName = myview.findViewById(R.id.name);
        final EditText mDesc = myview.findViewById(R.id.description);

        mName.setText(name);
        mName.setSelection(name.length());

        mDesc.setText(description);
        mDesc.setSelection(description.length());

        Button btnDelete = myview.findViewById(R.id.btndelete);
        Button btnUpdate = myview.findViewById(R.id.btnupdate);

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               name = mName.getText().toString().trim();
               description = mDesc.getText().toString().trim();
               String mDate = DateFormat.getDateInstance().format(new Date());

               Data data = new Data(name,description,post_key,mDate);
               mDatabase.child(post_key).setValue(data);

               dialog.dismiss();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatabase.child(post_key).removeValue();

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mainmenu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.logout:
                mAuth.signOut();
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
