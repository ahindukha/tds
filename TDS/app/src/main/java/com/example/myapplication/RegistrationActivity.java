package com.example.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvlogin;
    private Button btnsignup;
    private EditText etemail;
    private EditText reg_password;
    private EditText reg_password2;

    private ProgressDialog progressDialog;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        mAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);


        tvlogin = (TextView) findViewById(R.id.tvlogin);
        btnsignup = (Button) findViewById(R.id.signup);
        etemail = (EditText) findViewById(R.id.login_email);
        reg_password = (EditText) findViewById(R.id.reg_password);
        reg_password2 = (EditText) findViewById(R.id.reg_password2);

        tvlogin.setOnClickListener(this);
        btnsignup.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        if(v == tvlogin){
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
        }

        if(v == btnsignup){
            createAccount();
        }

    }

    public void createAccount() {

        String mEmail = etemail.getText().toString().trim();
        String mPassword = reg_password.getText().toString().trim();
        String mPassword2 = reg_password2.getText().toString().trim();

        if (TextUtils.isEmpty(mEmail)) {
            etemail.setError("Required Field");
            return;
        }
        if (TextUtils.isEmpty(mPassword)) {
            reg_password.setError("Required Field");
            return;
        }
        if (TextUtils.isEmpty(mPassword2)) {
            reg_password2.setError("Required Field");
            return;
        }

        if (!mPassword.equals(mPassword2)) {
            Toast.makeText(this, "Password and repeat password must match", Toast.LENGTH_SHORT).show();
        }
        else {
            progressDialog.setMessage("Registering...");
            progressDialog.show();
            mAuth.createUserWithEmailAndPassword(mEmail, mPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {

                    if (task.isSuccessful()) {
                        progressDialog.dismiss();
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    }

                }
            });
        }


    }


}
